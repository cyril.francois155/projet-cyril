import * as React from 'react';
import { Animated, Text,Button, View, StyleSheet, StatusBar } from 'react-native';

export function TestScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:'yellow' }}>
      <Text>Test Screen</Text>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}