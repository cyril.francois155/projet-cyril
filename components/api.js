import React, { useState } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { Card } from 'react-native-paper';
import { Text, Button } from 'react-native-elements';


import axios from 'axios';
import { max } from 'react-native-reanimated';

export default function Api(props) {

  const [data, setData] = useState( 
    async () => {
      const url = 'https://random-data-api.com/api/cannabis/random_cannabis?size=5';


      const result = await axios(url);
      setData(result.data);
      console.log("test loading data");
      console.log(result.data);
  });

  const renderItem = ({ item }) => (
    <View>
    <Card style={styles.card}>
    <Text>Variété : {item.strain}</Text>
    <Text>Catégorie : {item.category}</Text>
    <Text>Type : {item.type}</Text>
    </Card>
    </View>
  );

  const [isLoading, setIsLoading] = useState(true);

  return (
    <View style={styles.view}>
    
    <Text h1>
    Les variétés
    </Text>
    <FlatList style={styles.list}
        data={data}
        renderItem={renderItem}
      />
    
    <Button style={{paddingBottom: 15}}
    onPress={() => {window.location.reload()}}
    title="Voir d'autres variétés"
/>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'center',
  },

  list: {
    flex: 1,
    margin: 25,
  },

  card: {
    flex: 1,
    padding: 20,
    margin: 15,
    width: 300,
    textAlign: "center",
  },
});


